# RFID tagging and automatic billing

 

## Getting Started

This project let's you scan rfid tags using rfid reader and arduino. 

### Prerequisites


```
1) arduino hardware and arduino ide
2) MFRC522

```
## Running the tests
```
1) connect wire according to given code.
2) run code to scan tags and total cost of things.
```
## Result
![result.jpg](https://bitbucket.org/repo/p48BE5e/images/982103191-result.jpg)